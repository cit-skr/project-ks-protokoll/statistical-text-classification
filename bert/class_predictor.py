
from transformers import TFBertModel,  BertConfig, BertTokenizerFast, BertTokenizer

class GPT2Predict:
    def __init__(self, model_dir):

        # Load transformers config and set output_hidden_states to False
        config = BertConfig.from_pretrained(model_dir)
        config.output_hidden_states = False
        # Load BERT tokenizer
        self.tokenizer = BertTokenizerFast.from_pretrained(pretrained_model_name_or_path = model_dir, config = config)
        # Load the Transformers BERT model
        self.model = TFBertModel.from_pretrained(model_dir, config = config)


    def predict(self, text):
        input_ids = self.tokenizer.encode(text, return_tensors='tf')
        # getting out output
        beam_output = self.model.

        return self.tokenizer.decode(beam_output[0])

