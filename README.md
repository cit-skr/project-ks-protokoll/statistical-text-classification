# Statistical-Text-Classification

### front-end
- react, material-ui
- install dependencies: cd front-end && npm install
- run: cd front-end && npm start

### back-end
- falsk, transformers, tensorflow, torch
- install dependencies: cd back-end && pip install -r requirements.txt
- run: cd back-end && flask start


### Features for the bert senstence classifier
- Track the training data, maintain a csv file or a db
- trainig data path and train trigger needs to be loaded from the front end
- tracking the model, accuracy and loss
- predict functionality inside back-end/app.py get_class()

```
graphql database hasura:

http://ec2-13-49-27-116.eu-north-1.compute.amazonaws.com/console/api-explorer


from gql import gql, Client, AIOHTTPTransport, transport

   transport = AIOHTTPTransport(
        url="http://ec2-13-49-27-116.eu-north-1.compute.amazonaws.com/v1/graphql",
        headers={'Content-Type': 'application/json', 'x-hasura-admin-secret': 'cloud_runner'}
    )

    # Create a GraphQL client using the defined transport
    self.client = Client(transport=transport, fetch_schema_from_transport=True)


project_ks_protocoll/webscraper/ks_protokoll/pipelines.py

```


### Docker to run flask as applicatoin

`docker build -t skr:latest .`
`docker run -p 8081:6000 --rm --env-file=.env --name=skr skr`

to view content within docker container

`docker exec -it skr /bin/bash`


