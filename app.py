""" App configuration. """
import os, sys, dotenv, datetime, traceback, json, threading
project_dir = "statistical-text-classification"
parent_folder_path = os.path.dirname( os.path.abspath(__file__)).split(project_dir)[0]
sys.path.append(parent_folder_path)

# Find .env file
basedir = os.path.join(parent_folder_path,project_dir)
dotenv.load_dotenv(os.path.join(basedir, '.env'))

# General Config
NOW = datetime.datetime.now() 
FILE_DOWNLOAD_PATH = os.environ.get("FILE_DOWNLOAD_PATH")
HASURA_URL = os.environ.get("HASURA_URL")
HASURA_ADMIN_PASSWORD = os.environ.get("HASURA_ADMIN_PASSWORD")
TOKENIZER_LANGUAGE = 'swedish' # 'english'

import numpy as np
from flask import Flask, request
from gql import gql, Client, AIOHTTPTransport, transport
from classification.skr_predict_using_saved_checkpoints import predict_using_saved_checkpoints
from classification.skr_train import train_incremental
import asyncio
import json
# from bert.next_word_predictor import NextWordPredcitor
# from gpt2.custom.gpt2 import GPT2Predict

# bert_next_word_predictor = NextWordPredcitor()
# grpt2_predictor = GPT2Predict("./gpt2/custom/model_gpt2_sv") 

app = Flask(__name__)
# TRAIN_STATUS = {"accuracy":0,"loss":1,"progress":10}
os.environ["TRAIN_STATUS"] = json.dumps({"accuracy":0,"loss":0,"progress":0})


# sample: http://127.0.0.1:5000/bert/next_word_predictor?'{'input':"something"}'
# @app.route('/bert/next_word', methods = ['POST'])
# def get_next_word():
#     input_text = request.get_json()
#     print(input_text)
#     result = bert_next_word_predictor.predict(input_text['input'])
#     print(result)
#     return result



# @app.route('/gpt2/next_sentence', methods = ['POST'])
# def get_next_sentence_gpt2():
#     input_text = request.get_json()
#     print(input_text)
#     predictions = []
#     for _ in range(2):
#         predictions.append(str(grpt2_predictor.predict(input_text['input'])).strip())
#     result = {'predictions': predictions}
#     print(result)
#     return result

LABEL_DICT = json.load(open("./labels.json",'r'))

# from classification.skr_models import get_model, get_tokenizer
# model = get_model(restore_from_checkpoints=True)
# tokenizer = get_tokenizer()

@app.route('/bert/sentence_class', methods = ['POST'])
def get_class():
    input_text = request.get_json()
    sentences = [input_text['input']]
    result = predict_using_saved_checkpoints(sentences)
    # result is an array of array, content from index 0 is the predictions for the input sentence
    result = result[0]
    print(result)
    return {'predictions':result}



def train_callback():
    os.environ["TRAIN_STATUS"] = json.dumps({"accuracy":0,"loss":0,"progress":0})
    asyncio.set_event_loop(asyncio.SelectorEventLoop())
    asyncio.get_event_loop().run_until_complete(train_incremental())

@app.route('/bert/classifier/train', methods = ['POST','GET'])
def train():
    try:
        thread = threading.Thread(target=train_callback)
        thread.start()
        return {'status':'ok'}
    except Exception as e:
        return {'status':'fail','error':str(e)}




@app.route('/bert/classifier/train/status', methods = ['GET'])
def get_train_status():
    status = os.environ.get("TRAIN_STATUS")
    return {'train_status':json.loads(status)}


if __name__ == "__main__":
    app.run(host="0.0.0.0")