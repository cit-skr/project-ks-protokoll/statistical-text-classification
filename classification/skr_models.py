""" App configuration. """
import os, sys, dotenv, datetime, traceback, json
project_dir = "statistical-text-classification"
parent_folder_path = os.path.dirname( os.path.abspath(__file__)).split(project_dir)[0]

# Find .env file
basedir = os.path.join(parent_folder_path,project_dir)
sys.path.append(basedir)
dotenv.load_dotenv(os.path.join(basedir, '.env'))

# General Config
NOW = datetime.datetime.now() 
FILE_DOWNLOAD_PATH = os.environ.get("FILE_DOWNLOAD_PATH")
CHECKPOINT_PATH = os.environ.get("CHECKPOINT_PATH")
PRETRAINED_MODEL_PATH = os.environ.get("PRETRAINED_MODEL_PATH")
LOCAL_TOKENIZER_PATH = os.path.join(basedir,os.environ.get("LOCAL_TOKENIZER_PATH"))
NUMOFLABELS = os.environ.get("NUMOFLABELS")


check_point_file_path = os.path.join(basedir,os.path.join(CHECKPOINT_PATH,'cp.ckpt'))
checkpoints_path = os.path.join(basedir,CHECKPOINT_PATH)

import tensorflow as tf
import tensorflow_datasets as tfds
from transformers import BertTokenizer, TFBertForSequenceClassification, TFBertModel, BertConfig
import os
import os.path
import keras
from tensorflow.keras.layers import Input, Dropout, Dense
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.initializers import TruncatedNormal
from tensorflow.keras.losses import CategoricalCrossentropy
from tensorflow.keras.metrics import CategoricalAccuracy
from tensorflow.keras.utils import to_categorical
# And pandas for data import + sklearn because you allways need sklearn
import pandas as pd
from sklearn.model_selection import train_test_split



def get_tokenizer():
  if len(os.listdir(LOCAL_TOKENIZER_PATH)) > 0:
    tokenizer = BertTokenizer.from_pretrained(LOCAL_TOKENIZER_PATH, do_lower_case=True)
  else:
    tokenizer = BertTokenizer.from_pretrained(PRETRAINED_MODEL_PATH, do_lower_case=True)
    tokenizer.save_pretrained(LOCAL_TOKENIZER_PATH)
  print(tokenizer)
  return tokenizer

# can be up to 512 for BERT
max_length = 120
batch_size = 16


def convert_example_to_feature(tokenizer, review):

  # combine step for tokenization, WordPiece vector mapping and will add also special tokens and truncate reviews longer than our max length

  return tokenizer.encode_plus(review,
                add_special_tokens = True, # add [CLS], [SEP]
                max_length = max_length, # max length of the text that can go to BERT
                pad_to_max_length = True, # add [PAD] tokens
                return_attention_mask = True, # add attention mask to not focus on pad tokens
                truncation = True
              )


def encode_examples(tokenizer,sentences):
  # prepare list, so that we can build up final TensorFlow dataset from slices.
  input_ids_list = []
  token_type_ids_list = []
  attention_mask_list = []

  for review in sentences:
    bert_input = convert_example_to_feature(tokenizer, review)
    input_ids_list.append(bert_input['input_ids'])
    token_type_ids_list.append(bert_input['token_type_ids'])
    attention_mask_list.append(bert_input['attention_mask'])
  return input_ids_list, token_type_ids_list, attention_mask_list


def get_model(restore_from_checkpoints = False):
  # model initialization
  # Build your model input
  # model = TFBertModel.from_pretrained(PRETRAINED_MODEL_PATH)
  # model = TFBertForSequenceClassification.from_pretrained(PRETRAINED_MODEL_PATH)
  transformer_model = TFBertModel.from_pretrained(PRETRAINED_MODEL_PATH)
  bert = transformer_model.layers[0]
  config = BertConfig.from_pretrained(PRETRAINED_MODEL_PATH)
  config.output_hidden_states = False


  # Build your model input
  input_ids = Input(shape=(max_length,), name='input_ids', dtype='int32')
  inputs = {'input_ids': input_ids}
  # Load the Transformers BERT model as a layer in a Keras model
  bert_model = bert(inputs)[1]
  dropout = Dropout(config.hidden_dropout_prob, name='pooled_output')
  pooled_output = dropout(bert_model, training=False)
  # Then build your model output
  decision = Dense(units=NUMOFLABELS, kernel_initializer=TruncatedNormal(stddev=config.initializer_range), name='decision')(pooled_output)
  outputs = {'decision': decision}
  # And combine it all in a model object
  model = Model(inputs=inputs, outputs=outputs, name='BERT_MultiLabel_MultiClass')
  # Take a look at the model
  model.summary()

  optimizer = tf.keras.optimizers.Adam(learning_rate=5e-5, epsilon=1e-08,decay=0.01,clipnorm=1.0)
  # we do not have one-hot vectors, we can use sparce categorical cross entropy and accuracy
  # loss = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
  # metric = tf.keras.metrics.SparseCategoricalAccuracy('accuracy')

  loss = {'decision': tf.keras.losses.SparseCategoricalCrossentropy(from_logits = True)}
  metric = {'decision': tf.keras.metrics.SparseCategoricalAccuracy('accuracy')}


  model.compile(optimizer=optimizer, loss=loss, metrics=metric)
  #todo, if checkpoint exists, load weights, so training becomes incremental
  if restore_from_checkpoints == True and len(os.listdir(checkpoints_path)) > 0:
    print("loading weights..............................." + check_point_file_path)
    model.load_weights(check_point_file_path)
  return model



