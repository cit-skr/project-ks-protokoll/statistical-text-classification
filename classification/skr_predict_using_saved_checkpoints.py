""" App configuration. """
import os, sys, dotenv, datetime, traceback, json
project_dir = "statistical-text-classification"
parent_folder_path = os.path.dirname( os.path.abspath(__file__)).split(project_dir)[0]


# Find .env file
basedir = os.path.join(parent_folder_path,project_dir)
sys.path.append(basedir)
dotenv.load_dotenv(os.path.join(basedir, '.env'))

# General Config
NOW = datetime.datetime.now() 
FILE_DOWNLOAD_PATH = os.environ.get("FILE_DOWNLOAD_PATH")
CHECKPOINT_PATH = os.environ.get("CHECKPOINT_PATH")

checkpoints_path = os.path.join(basedir,CHECKPOINT_PATH)
check_point_file_path = os.path.join(basedir,os.path.join(CHECKPOINT_PATH,'cp.ckpt'))

import tensorflow as tf
from classification.skr_models import get_model, get_tokenizer, encode_examples
from scipy.special import softmax
from numpy import round
# from hasura_services import get_labels


MODEL = None

def predict_using_saved_checkpoints(sentences):
  # train dataset
  tokenizer = get_tokenizer()
  # labels value here is just dummy, which is used by the predict method from restored model
  input_ids_list, token_type_ids_list, attention_mask_list = encode_examples(tokenizer, sentences)
  global MODEL
  if MODEL is not None and len(os.listdir(checkpoints_path)) > 0:
    print('reuse loaded model...........')
    MODEL.load_weights(check_point_file_path)
  else:
    print('reload model...........')
    MODEL = get_model(restore_from_checkpoints=True)


  # model.load_weights(CHECKPOINT_PATH)
  predictions = MODEL.predict(input_ids_list)['decision']
  normalized_predictions = round(softmax(predictions, axis=1) * 100,decimals=0)
  label_dic = json.load(open(os.path.join(basedir,"labels.json"),'r'))
  result = []
  # the predictions looks like [[17, 12], [12,14]]
  # and here it is converted to [{'label1':17,'label2':12}, {'label1':12,'label2':14}]
  for m in range(len(normalized_predictions)):
    predictions_one_sentence = normalized_predictions[m]
    result_one_sentence = {}
    for n in range(len(predictions_one_sentence)):
      key = label_dic[str(n)]
      value = int(predictions_one_sentence[n])
      result_one_sentence[key] = value
    result.append(result_one_sentence)

  return result


if __name__=="__main__":
  sentences = ['Information om Halmstads kommuns arbete med COVID-19','Kommundirektören informerar kommunstyrelsen kring Halmstads kommuns hantering av arbetet med coronaviruset COVID-19.']
  # model = get_model(restore_from_checkpoints=True)
  # tokenizer = get_tokenizer()
  result = predict_using_saved_checkpoints(sentences)
  print(result)
