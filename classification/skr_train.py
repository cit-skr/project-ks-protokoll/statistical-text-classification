""" App configuration. """
import os, sys, dotenv, datetime, traceback, json
project_dir = "statistical-text-classification"
parent_folder_path = os.path.dirname( os.path.abspath(__file__)).split(project_dir)[0]

# Find .env file
basedir = os.path.join(parent_folder_path,project_dir)
sys.path.append(basedir)
dotenv.load_dotenv(os.path.join(basedir, '.env'))

# General Config
NOW = datetime.datetime.now() 
FILE_DOWNLOAD_PATH = os.environ.get("FILE_DOWNLOAD_PATH")
CHECKPOINT_PATH = os.environ.get("CHECKPOINT_PATH")
LOCAL_TOKENIZER_PATH = os.environ.get("LOCAL_TOKENIZER_PATH")


check_point_file_path = os.path.join(basedir,os.path.join(CHECKPOINT_PATH,'cp.ckpt'))
import json
import tensorflow as tf
import tensorflow_datasets as tfds
from transformers import BertTokenizer, TFBertForSequenceClassification
from classification.skr_models import get_tokenizer, get_model, encode_examples
from hasura_services import get_training_data_from_db
import numpy as np

EPOCHS = 20
BATCH_SIZE = 5

# logger to set job status
class LossAndErrorPrintingCallback(tf.keras.callbacks.Callback):

    current_epoch = None
    def on_epoch_begin(self, epoch, logs=None):
        self.current_epoch = epoch


    def on_train_batch_end(self, batch, logs=None):
        accuracy = logs["accuracy"]
        loss = logs["loss"]
        self.__set_train_status(accuracy,loss, self.current_epoch+round(batch/BATCH_SIZE,1))

    def on_test_batch_end(self, batch, logs=None):
        accuracy = logs["accuracy"]
        loss = logs["loss"]
        self.__set_train_status(accuracy,loss, self.current_epoch)

    def on_epoch_end(self, epoch, logs=None):
        # example of logs
        # {
        #   'loss': 1.5582252740859985,
        #   'accuracy': 0.30000001192092896,
        #   'val_loss': 1.4481701850891113,
        #   'val_accuracy': 0.3181818127632141
        # }
        accuracy = logs["accuracy"]
        loss = logs["loss"]
        self.__set_train_status(accuracy,loss, epoch)

    def __set_train_status(self,accuracy, loss, epoch):
        global EPOCHS
        process = round(epoch/EPOCHS * 100)
        os.environ["TRAIN_STATUS"] = json.dumps({"accuracy":round(accuracy,4),"loss":round(loss,4),"progress":process})





def get_history_file_name():
    x = datetime.datetime.now().replace(second=0, microsecond=0)
    return str(x).replace(" ", "-") + ".json"

# example of training_data_path "Extract_Covid_19_20200916.xlsx"
async def train_incremental():
    global BATCH_SIZE, EPOCHS, LOCAL_TOKENIZER_PATH

    history_path = os.path.join(basedir,"classification/history")
    tokenizer_path = os.path.join(basedir,LOCAL_TOKENIZER_PATH)
    checkpoints_path = os.path.join(basedir,CHECKPOINT_PATH)
    # create some folders if not exist
    if os.path.isdir(history_path) == False:
        os.mkdir(history_path)
    if os.path.isdir(tokenizer_path) == False:
        os.mkdir(tokenizer_path)
    if os.path.isdir(checkpoints_path) == False:
        os.mkdir(checkpoints_path)


    sentences, labels, r_dict = get_training_data_from_db()
    # json.dump(r_dict,open(os.path.join(basedir, 'labels.json'),'w'),indent=4)

    # train dataset, 80%
    total = len(sentences)
    training_num = int(total * 0.9)
    tokenizer = get_tokenizer()
    input_ids_list_train, token_type_ids_list_train, attention_mask_list_train = encode_examples(tokenizer, sentences)
    # callback to save checkpoint
    cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=check_point_file_path,
                                                    save_weights_only=True,
                                                    verbose=1)
    model = get_model(True)
    # history = model.fit(ds_train_encoded, epochs = EPOCHS, validation_data=ds_test_encoded, callbacks=[cp_callback])
    history = model.fit(x={'input_ids': np.array(input_ids_list_train)}, y={'decision': np.array(labels)}, epochs = EPOCHS, validation_split=0.2, callbacks=[LossAndErrorPrintingCallback(),cp_callback])
    # dump history to a folder
    f = open( os.path.join( history_path, get_history_file_name()), "x")
    f.write(json.dumps(history.history))
    f.close()

    # Reset env variables
    os.environ["TRAIN_STATUS"] = json.dumps({"accuracy":0,"loss":0,"progress":0})

    # only for testing purpose to verify loaded classifier yields same result as the trained one
    result = model.predict(np.array(input_ids_list_train))
    print(result)


# model = get_model(True)
# result = model.predict(test_encoded)
# print(result)



if __name__=="__main__":
    TRAIN_STATUS = {"accuracy":0,"loss":1,"progress":10}
    import asyncio
    asyncio.get_event_loop().run_until_complete(train_incremental())


