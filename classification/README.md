# Statistical-Text-Classification


## set up and run script

### set up python env
https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/26/python-virtual-env/

and 

`source "path_to_virtual_env/bin/activate"`

### download cuda
https://medium.com/@sh.tsang/tutorial-cuda-v10-2-cudnn-v7-6-5-installation-ubuntu-18-04-3d24c157473f

### install python packages into virtualenv
pip install --use-feature=2020-resolver -q tf-nightly
pip install --use-feature=2020-resolver -q tf-models-nightly
pip install pydot
sudo apt-get install python-pydot

at this stage all lib required are installed in the virtual env


### training

training happens incrementally, each training saves the updated weights to `CHECKPOINT_PATH = "./checkpoints/cp.ckpt"`, to start a new training session, you can run the script with one parameter specifying file path for input training data, ex.

```
from skr_train import *
train_incremental("Extract_Covid_19_20200916.xlsx")
```

after it finishes, you should see similar trace as below

```
1/1 [==============================] - 17s 17s/step - loss: 0.8571 - accuracy: 0.3043 - val_loss: nan - val_accuracy: 0.6957
Epoch 2/3
1/1 [==============================] - 8s 8s/step - loss: nan - accuracy: 0.6957 - val_loss: nan - val_accuracy: 0.6957
Epoch 3/3
1/1 [==============================] - 8s 8s/step - loss: nan - accuracy: 0.6957 - val_loss: nan - val_accuracy: 0.6957
```


### prediction

to use the model for prediction, run ex. following
```
from skr_predict_using_saved_checkpoints import *

sentences = ['Information om Halmstads kommuns arbete med COVID-19','Kommundirektören informerar kommunstyrelsen kring Halmstads kommuns hantering av arbetet med coronaviruset COVID-19.']
labels = [0,0]
predict_using_saved_checkpoints(sentences, labels)


```

worth to know that `labels` is just a dummy parameter, here just send in all `0`

### usage of models

there are two major parts with the modeling - tokeniser and classifier





this should support 104 languages
https://tfhub.dev/tensorflow/bert_multi_cased_L-12_H-768_A-12/2
and all other saved models are here
https://tfhub.dev/google/collections/bert/1



visualize loss
https://www.depends-on-the-definition.com/named-entity-recognition-with-bert/