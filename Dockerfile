# FROM ubuntu:18.04

FROM python:3.6

ENV SHELL=/bin/bash

LABEL maintainer="marc.chen@chalmersindustriteknik.se"

RUN apt-get update -y && apt-get install -y cmake protobuf-compiler

RUN mkdir /statistical-text-classification
WORKDIR /statistical-text-classification

# We copy just the requirements.txt first to leverage Docker cache
COPY ./requirements.txt /statistical-text-classification/requirements.txt

RUN pip install -r requirements.txt --use-feature=2020-resolver


