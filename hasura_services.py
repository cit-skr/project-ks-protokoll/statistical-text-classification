""" App configuration. """
import os, sys, dotenv, datetime, traceback
project_dir = "statistical-text-classification"
parent_folder_path = os.path.dirname( os.path.abspath(__file__)).split(project_dir)[0]
sys.path.append(parent_folder_path)

# Find .env file
basedir = os.path.join(parent_folder_path,project_dir)
dotenv.load_dotenv(os.path.join(basedir, '.env'))

# General Config
NOW = datetime.datetime.now() 
FILE_DOWNLOAD_PATH = os.environ.get("FILE_DOWNLOAD_PATH")
HASURA_URL = os.environ.get("HASURA_URL")
HASURA_ADMIN_PASSWORD = os.environ.get("HASURA_ADMIN_PASSWORD")
TOKENIZER_LANGUAGE = 'swedish' # 'english'


import argparse, os, re, base64, json, time
from tqdm import tqdm

import numpy as np
import pandas as pd

import nltk
nltk.download('punkt')
from nltk.tokenize import sent_tokenize

# import gql as gql_main
# import aiohttp
# from gql import gql, Client, transport, AIOHTTPTransport

from sgqlc.endpoint.http import HTTPEndpoint



insert_sentence = """
    mutation {
        insert_ks_protokoll_schema_extracted_sentences_one(object: {municipality_id: %d, meeting_date: "%s", text: "%s", label_id: %d, search_query: "%s", label: "%s"}) {
            municipality_id
            meeting_date
            created_on
            sentence_id
            text
            label_id
            search_query
        }
    }
"""

get_extracted_sentences = """
    query MyQuery {
        ks_protokoll_schema_extracted_sentences(order_by: {usage_count: asc}) {
            sentence_id
            municipality_id
            meeting_date
            file_id
            label_id
            label
            text
            usage_count
        }
    }
"""

update_usage = """
    mutation update($sentence_id: Int! = %d) {
        update_ks_protokoll_schema_extracted_sentences_by_pk(pk_columns: {sentence_id: $sentence_id}) {
            usage_count
        }
    }
"""

insert_label = """
    mutation {
        insert_ks_protokoll_schema_sentence_labels_one(object: {label: "%s", sub_label: "%s"}) {
            id
            label
            sub_label
        }
    }

"""

get_label_id = """
    query MyQuery{
        ks_protokoll_schema_sentence_labels(where: {label: {_similar: "%s*"}}) {
            id
            sub_label
        }
    }
"""

set_sub_label = """
    mutation {
        update_ks_protokoll_schema_sentence_labels_by_pk(pk_columns: {id: %d}, _set: {sub_label: "%s"}) {
            id
        }
    }
"""

get_unique_labels = """
    query MyQuery {
        ks_protokoll_schema_extracted_sentences(distinct_on: label) {
            label
        }
    }
"""


def get_gql_client():
    global HASURA_URL
    global HASURA_ADMIN_PASSWORD
    # _transport = AIOHTTPTransport(
    #     url= HASURA_URL,
    #     headers={'Content-Type': 'application/json', 'x-hasura-admin-secret': HASURA_ADMIN_PASSWORD }
    # )
    # # Create a GraphQL client using the defined transport
    # client = Client(transport=_transport, fetch_schema_from_transport=True)
    url = 'http://ec2-13-49-27-116.eu-north-1.compute.amazonaws.com:5000/v1/graphql'
    headers = {'Content-Type': 'application/json'}
    client = HTTPEndpoint(url, headers)
    return client

def execute_gql(client, query):
    r = None
    try:
        r = client(query)
        return r
    except gql_main.transport.exceptions.TransportQueryError as e:
        if "Uniqueness violation" not in str(e):
            print(e)
            traceback.print_exc()
        return r
    except BaseException as e:
        print(e)
        return r


def insert_one_sentence( municipality_id, meeting_date, text, label, search_query, sub_label=""):
    gql_client = get_gql_client()
    
    # insert label
    label_id = 1
    sub_label_old = None
    response = execute_gql(gql_client, get_label_id % (label))
    if response:
        r_label = response["ks_protokoll_schema_sentence_labels"]
        if len(r_label)>0:
            label_id = r_label[0].get("id")
            sub_label_old = r_label[0].get("sub_label") 
    if sub_label_old:
        if sub_label.strip() not in  sub_label_old:
            new_sub_label = sub_label_old + ', ' + sub_label 
            response = execute_gql(gql_client, set_sub_label % (label_id,new_sub_label))
            if response:
                r_label = response["update_ks_protokoll_schema_sentence_labels_by_pk"]
                label_id = r_label.get("id")
    else:
        new_sub_label = sub_label
        response = execute_gql(gql_client, insert_label % (label,new_sub_label))
        if response:
            r_label = response["insert_ks_protokoll_schema_sentence_labels_one"]
            label_id = r_label.get("id")


    # insert sentence
    response = execute_gql(gql_client, insert_sentence % (municipality_id, meeting_date, text, label_id, search_query,label))

    return response


def get_municipality_id(municipality_name):
    query = """
        query MyQuery($_similar: String = "%s*") {
            ks_protokoll_schema_municipality_info(where: {name: {_similar: $_similar}}) {
                municipality_id
                name
            }
        }
    """
    gql_client = get_gql_client()

    response = execute_gql(gql_client, query % (municipality_name))
    r_m_info = response["ks_protokoll_schema_municipality_info"]
    m_id = 0
    if len(r_m_info)>0:
        m_id = r_m_info[0].get("municipality_id")

    return m_id


def extract_excel_and_insert(path):
    if os.path.exists(path):
        df_with_null = pd.read_excel(path, sheet_name='Extracted_sentences')
        df = df_with_null[df_with_null.Sentence.isnull() == False]
        df = df_with_null[df_with_null.Aspect_decision.isnull() == False]

        columns = ["Municipality", "Date", "Search word", "Sentence", "Aspect_decision"]
      
        for i, item in df[columns].iterrows():
            try:
                municipality = item["Municipality"]
                date = str(item["Date"]).split(' ')[0]
                search_query = item["Search word"] 
                text = item["Sentence"]
                a_decision = item["Aspect_decision"].split('(')
                label = a_decision[0].strip().replace("_",'-').replace(' ','-')
                label = label.replace("Decison", "Decision").replace("assessement","assessment")
                sub_label = a_decision[1].replace(')','') if len(a_decision)>1 else ""

                m_id = get_municipality_id(municipality)
                print(m_id, municipality, date, search_query, label, sub_label, text[:20]+"...")
                r = insert_one_sentence(m_id, date, text, label, search_query,sub_label=sub_label)
                print(r)
            except BaseException as e:
                traceback.print_exc()
                print(e)

    else:
        raise f"path does not exist: {path}"

def get_training_data_from_db(label_dict=None):
    gql_client = get_gql_client()
    r = gql_client(get_unique_labels)
    if not label_dict:
        label_dict = {item.get('label'):i for i, item in enumerate(r['data']["ks_protokoll_schema_extracted_sentences"])}
    r = gql_client(get_extracted_sentences)
    sentences = []; labels = []
    if r:
        data = r['data']["ks_protokoll_schema_extracted_sentences"]
        for item in data:
            sentences.append(item.get("text"))
            label = item.get("label")
            labels.append(label_dict[label])

    reverse_label_dict = {v:k for k,v in label_dict.items()}
    return sentences, labels, reverse_label_dict

# todo(marc) write a new gql to readout the labels
# example of label dic
# {0: 'Cirkulär', 1: 'Decision', 2: 'Information', 3: 'Kommunala-vuxenutbildningen', 4: 'Överenskommelse', 5: 'Pre-decision', 6: 'Situation-assessment'}
def get_labels():
    return {0: 'Cirkulär', 1: 'Decision', 2: 'Information', 3: 'Kommunala-vuxenutbildningen', 4: 'Överenskommelse', 5: 'Pre-decision', 6: 'Situation-assessment'}





if __name__=='__main__':
    # path = "/mnt/InternalStorage/sidkas/project_ks_protocoll/statistical-text-classification/assets/Extract_Covid_19_20200916.xlsx"
    # extract_excel_and_insert(path)
    import asyncio
    s, l , r_dict = asyncio.get_event_loop().run_until_complete(get_training_data_from_db())
    print(r_dict)
    # for item in zip(s,l):
    #     print(item)
    # json.dump(r_dict,open('labels.json','w'),indent=4)
